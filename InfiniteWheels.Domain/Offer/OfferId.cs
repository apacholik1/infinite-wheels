using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Offer;

public class OfferId : Value<OfferId>
{
    public Guid Value { get; private set; }
    
    protected OfferId() {}

    public OfferId(Guid value)
    {
        if (value == default)
        {
            throw new ArgumentNullException(
                nameof(value), "Id kontraktu nie może być puste"
            );
        }

        Value = value;
    }

    public static implicit operator Guid( OfferId self ) => self.Value;
}