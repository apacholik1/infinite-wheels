﻿namespace InfiniteWheels.Domain.Offer
{
    public static class Events
    {
        public class OfferCreated
        {
            public Guid Id { get; set; }
            public Guid ClientId { get; set; }
        }

        public class OfferUpdateRentalDuration
        {
            public Guid Id { get; set; }
            public ushort RentalDuration { get; set; }
        }

        public class OfferAccept
        {
            public Guid Id { get; set; }
        }

        public class OfferUpdateLiftingCapacity
        {
            public Guid Id { get; set; }
            public uint LiftingCapacity { get; set; }
        }
    }
}
