using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Offer;

public class RentalDuration: Value<RentalDuration>
{
    public ushort Value { get; private set; } 
    
    protected RentalDuration() {}

    protected RentalDuration(ushort value) {
        if (value == 0)
        {
            throw new ArgumentException(
                "Czas wypo�yczenia nie mo�e by� mniejszy ni� 1", nameof(value)
            );
        }
        
        Value = value;
    }

    public static implicit operator ushort(RentalDuration self) => self.Value;

    public static RentalDuration FromUshort(ushort value) => new RentalDuration(value);
}