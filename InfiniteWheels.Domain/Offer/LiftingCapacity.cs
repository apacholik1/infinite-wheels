using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Offer;

public class LiftingCapacity : Value<LiftingCapacity>
{
    public const uint maxValue = 1_000_000;
    
    public uint Value { get; private set; }
    
    protected LiftingCapacity() {}
    
    public LiftingCapacity(uint capacity)
    {
        if (capacity <= 0)
        {
            throw new ArgumentException(
                "Udźwig nie może być mniejszy niż 0",
                nameof(capacity));
        }
        
        if (capacity > maxValue)
        {
            throw new ArgumentException(
                "Udźwig nie może być mniejszy niż 1 000 ton",
                nameof(capacity));
        }

        Value = capacity;
    }

    public static implicit operator uint(LiftingCapacity self) => self.Value;
}