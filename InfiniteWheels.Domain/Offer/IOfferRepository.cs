namespace InfiniteWheels.Domain.Offer;

public interface IOfferRepository
{
    Task<bool> Exists(Guid id);

    Task<Offer> Load(Guid id);

    Task Add(Offer entity);
}