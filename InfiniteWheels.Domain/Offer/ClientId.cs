using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Offer;

public class ClientId : Value<ClientId>
{
    public Guid Value { get; private set; }
    
    protected ClientId() {}

    public ClientId(Guid value)
    {
        if (value == default)
        {
            throw new ArgumentNullException(
                nameof(value), "Id klienta nie może być puste"
            );
        }

        Value = value;
    }

    public static implicit operator Guid( ClientId self ) => self.Value;
}