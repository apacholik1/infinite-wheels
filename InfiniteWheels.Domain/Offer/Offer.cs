using InfiniteWheels.Domain.Shared;
using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Offer;

public class Offer : Entity
{
    public Guid OfferId { get; private set; }
    public OfferId Id { get; private set; }
    public ClientId ClientId { get; private set; }
    public OfferStatus State { get; private set; }
    public RentalDuration? Duration { get; private set; } = null;
    public LiftingCapacity? LiftingCapacity { get; private set; } = null;
    
    protected Offer() {}

    public Offer(OfferId id, ClientId clientId)
    {
        Apply(new Events.OfferCreated { 
            ClientId = clientId,
            Id = id,
        });
    }

    public void SetRentalDuration(ushort rentalDuration) => Apply(new Events.OfferUpdateRentalDuration
    {
        Id = Id,
        RentalDuration = rentalDuration,
    });

    public void SetLiftingCapacity(uint liftingCapacity) => Apply(new Events.OfferUpdateLiftingCapacity
    {
        Id = Id,
        LiftingCapacity = liftingCapacity,
    });

    public void Accept() => Apply(new Events.OfferAccept
    {
        Id = Id
    });

    protected override void When(object @event)
    {
        switch (@event)
        {
            case Events.OfferCreated e:
                Id = new OfferId(e.Id);
                ClientId = new ClientId(e.ClientId);
                OfferId = e.Id;
                State = OfferStatus.Draft;
                break;

            case Events.OfferUpdateRentalDuration e:
                Duration = RentalDuration.FromUshort(e.RentalDuration);
                break;

            case Events.OfferAccept e:
                State = OfferStatus.Accepted;
                break;
            
            case Events.OfferUpdateLiftingCapacity e:
                LiftingCapacity = new LiftingCapacity(e.LiftingCapacity);
                break;
        }
    }

    protected override void EnsureValidState()
    {
        var valid =
            Id != null &&
            ClientId != null &&
            (State switch
            {
                // OfferStatus.Draft =>
                //     _rentalDuration != default,
                OfferStatus.Accepted =>
                    Duration != default &&
                    LiftingCapacity != default,
                _ => true
            });

        if (!valid)
            throw new DomainExceptions.InvalidEntityStateException(this, $"Kontrole końcowe zakończyły się niepowodzeniem w stanie {State}");
    }

    public enum OfferStatus
    {
        Draft = 1,
        Canceled = 2,
        Accepted = 3,
    }
    
    // TODO: fields to be changed to value objects
    // private string _typeOfEngines;
    // private int _heightRestrictions;
    // private string _specializedAttachments;
    // private string _worksiteRequirementsAndLimitations;
    // private string _workingEnvironmentDescription;
    // private string _operationFrequency;
    // private string _deliveryAndPickupLocation;
    // private string _additionalServices;
    // private string _budgetForForkliftRental;
}