namespace InfiniteWheels.Domain.Shared;

public static class DomainExceptions
{
    public class InvalidEntityStateException : Exception
    {
        public InvalidEntityStateException(object entity, string message)
            : base($"Zmiana stanu encji {entity.GetType().Name} została odrzucona, {message}")
        {
        }
    }
}