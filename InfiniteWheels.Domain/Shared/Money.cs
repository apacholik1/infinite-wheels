using System.Globalization;
using InfiniteWheels.Framework;

namespace InfiniteWheels.Domain.Shared;

public class Money : Value<Money>
{
    
    public static Money FromDecimal(decimal amount, string currency, ICurrencyLookup currencyLookup) => 
        new Money(amount, currency, currencyLookup);
    public static Money FromString(string amount, string currency, ICurrencyLookup currencyLookup) => 
        new Money(decimal.Parse(amount, CultureInfo.InvariantCulture), currency, currencyLookup);
    
    protected Money(decimal amount, string currencyCode, ICurrencyLookup currencyLookup)
    {
        if (string.IsNullOrEmpty(currencyCode))
            throw new ArgumentNullException(
                nameof(currencyCode),
                "Kod waluty musi być określony");

        var currency = currencyLookup.FindCurrency(currencyCode);

        if (!currency.InUse)
            throw new ArgumentException(
                $"Waluta {currencyCode} nie jest prawidłowa");
        
        if (decimal.Round(amount, currency.DecimalPlaces) != amount)
            throw new ArgumentOutOfRangeException(
                nameof(amount),
                $"Kwota w {currencyCode} nie może mieć więcej miejsc po przecinku niż {currency.DecimalPlaces}");
        
        Amount = amount;
        Currency = currency;
    }

    private Money(decimal amount, Currency currency)
    {
        Amount = amount;
        Currency = currency;
    }

    public decimal Amount { get; }
    public Currency Currency { get; }

    public Money Add(Money summand)
    {
        if (Currency != summand.Currency)
            throw new CurrencyMismatchException(
                "Nie można sumować kwot w różnych walutach");

        return new Money(Amount + summand.Amount, Currency);
    }

    public Money Subtract(Money subtrahend)
    {
        if (Currency != subtrahend.Currency)
            throw new CurrencyMismatchException(
                "Nie można odejmować kwot w różnych walutach");
        
        return new Money(Amount - subtrahend.Amount, Currency);
    }
    public static Money operator +(Money summand1, Money summand2) => summand1.Add(summand2);
    public static Money operator -(Money summand1, Money summand2) => summand1.Subtract(summand2);
    
    public override string ToString() => $"{Currency.CurrencyCode} {Amount}";
}

public class CurrencyMismatchException : Exception
{
    public CurrencyMismatchException(string message) : base(message)
    {
    }
}