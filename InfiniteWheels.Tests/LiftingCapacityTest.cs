using FluentAssertions;
using InfiniteWheels.Domain.Offer;

namespace InfiniteWheels.Tests;

public class LiftingCapacityTest
{
    [Fact]
    public void Lift_capacity_can_be_given_in_tons()
    {
        // Arrange
        uint oneTon = 1000;
        uint value = 5 * oneTon;

        // Act
        LiftingCapacity lc = new(value);

        // Assert
        Assert.Equal(value, lc);
    }

    [Fact]
    public void Lift_capacity_can_not_be_under_one_kg()
    {
        // Act
        var exception = Record.Exception(() => new LiftingCapacity(0));
        
        // Assert
        exception.Should().BeOfType<ArgumentException>();
    }

    [Fact]
    public void Lifting_capacity_can_not_be_greater_than_thousand_tons()
    {
        // Arrage
        uint oneTone = 1_000;
        uint thousandTons = oneTone * 1_000;
        uint moreThanThousandTons = thousandTons + 1;
        
        // Act
        var exception = Record.Exception(() => new LiftingCapacity(moreThanThousandTons));
        
        // Assert
        exception.Should().BeOfType<ArgumentException>();
    }
}