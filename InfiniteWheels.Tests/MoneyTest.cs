using FluentAssertions;
using InfiniteWheels.Domain.Shared;

namespace InfiniteWheels.Tests;

public class FakeCurrencyLookup : ICurrencyLookup
{
    private static readonly IEnumerable<Currency> _currencies = new[]
    {
        new Currency
        {
            CurrencyCode = "EUR",
            DecimalPlaces = 2,
            InUse = true
        },
        new Currency
        {
            CurrencyCode = "USD",
            DecimalPlaces = 2,
            InUse = true
        },
        new Currency
        {
            CurrencyCode = "JPY",
            DecimalPlaces = 0,
            InUse = true
        },
        new Currency
        {
            CurrencyCode = "DEM",
            DecimalPlaces = 2,
            InUse = false
        }
    };

    public Currency FindCurrency(string currencyCode)
    {
        var currency = _currencies.FirstOrDefault(
            x => x.CurrencyCode == currencyCode);

        return currency ?? Currency.None;
    }
}

public class MoneyTest
{
    private static readonly ICurrencyLookup CurrencyLookup =
        new FakeCurrencyLookup();
    
    [Fact]
    public void Two_of_same_amount_should_be_equal()
    {
        // Act
        var firstAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);
        var secondAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);

        // Assert
        Assert.Equal(firstAmount, secondAmount);
    }
    
    [Fact]
    public void Two_of_same_amount_but_differentCurrencies_should_not_be_equal()
    {
        // Act
        var firstAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);
        var secondAmount = Money.FromDecimal(5, "USD",
            CurrencyLookup);

        // Assert
        Assert.NotEqual(firstAmount, secondAmount);
    }
    
    [Fact]
    public void FromString_and_FromDecimal_should_be_equal()
    {
        // Act
        var firstAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);
        var secondAmount = Money.FromString("5.00", "EUR",
            CurrencyLookup);

        // Assert
        Assert.Equal(firstAmount, secondAmount);
    } 

    
    [Fact]
    public void Money_objects_with_the_same_amount_should_be_equal()
    {
        // Act
        var firstAmount = Money.FromDecimal(5, "EUR", CurrencyLookup);
        var secondAmount = Money.FromDecimal(5, "EUR", CurrencyLookup);
        
        // Assert
        Assert.Equal(firstAmount, secondAmount);
    }

    [Fact]
    public void Sum_of_money_gives_full_amount()
    {
        // Act
        var coin1 = Money.FromDecimal(1, "EUR", CurrencyLookup);
        var coin2 = Money.FromDecimal(2, "EUR", CurrencyLookup);
        var coin3 = Money.FromDecimal(2, "EUR", CurrencyLookup);
        var banknote = Money.FromDecimal(5, "EUR", CurrencyLookup);
        
        // Assert
        Assert.Equal(banknote, coin1 + coin2 + coin3);
    } 
    
    [Fact]
    public void Unused_currency_should_not_be_allowed()
    {
        // Act
        var exception = Record.Exception(() =>
            Money.FromDecimal(100, "DEM", CurrencyLookup)
        );

        // Assert
        exception.Should().BeOfType<ArgumentException>();
    }

    [Fact]
    public void Unknown_currency_should_not_be_allowed()
    {
        // Act
        var exception = Record.Exception(() =>
            Money.FromDecimal(100, "CO?", CurrencyLookup)
        );
        
        // Assert
        exception.Should().BeOfType<ArgumentException>();
    }

    [Fact]
    public void Throw_when_too_many_decimal_places()
    {
        // Act
        var exception = Record.Exception(() =>
            Money.FromDecimal(100.123m, "EUR", CurrencyLookup)
        );
        
        // Assert
        exception.Should().BeOfType<ArgumentOutOfRangeException>();
    } 
    
    [Fact]
    public void Throws_on_adding_different_currencies()
    {
        // Arrange
        var firstAmount = Money.FromDecimal(5, "USD",
            CurrencyLookup);
        var secondAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);
        
        // Act
        var exception = Record.Exception(() =>
            firstAmount + secondAmount
        );
        
        //Assert
        exception.Should().BeOfType<CurrencyMismatchException>();
    }

    [Fact]
    public void Throws_on_substracting_different_currencies()
    {
        // Arrange
        var firstAmount = Money.FromDecimal(5, "USD",
            CurrencyLookup);
        var secondAmount = Money.FromDecimal(5, "EUR",
            CurrencyLookup);
        
        // Act
        var exception = Record.Exception(() => firstAmount - secondAmount);
        
        // Assert
        exception.Should().BeOfType<CurrencyMismatchException>();
    } 

}