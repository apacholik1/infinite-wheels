﻿using FluentAssertions;
using InfiniteWheels.Domain.Offer;
using InfiniteWheels.Domain.Shared;

namespace InfiniteWheels.Tests
{
    public class Offer_Ready_Spec
    {
        private readonly Offer _offer;

        public Offer_Ready_Spec()
        {
            _offer = new Offer(new OfferId(Guid.NewGuid()), new ClientId(Guid.NewGuid()));
        }

        [Fact]
        public void Correct_offer_can_be_accepted()
        {
            // Arrange
            _offer.SetRentalDuration(100);
            _offer.SetLiftingCapacity(10_000);
            
            // Act
            _offer.Accept();

            // Assert
            Assert.Equal(Offer.OfferStatus.Accepted, _offer.State);
        }

        [Fact]
        public void Without_duration_can_not_be_ready()
        {
            // Act
            var exception = Record.Exception(() => _offer.Accept());
            
            // Assert
            exception.Should().BeOfType<DomainExceptions.InvalidEntityStateException>();
        }
    }
}
