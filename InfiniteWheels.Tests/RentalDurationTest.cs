using FluentAssertions;
using InfiniteWheels.Domain.Offer;

namespace InfiniteWheels.Tests;

public class RentalDurationTest
{
    [Fact]
    public void Rental_can_last_up_to_5_years()
    {
        // Arrange
        ushort years5 = 5 * 365;

        // Act
        RentalDuration rd = RentalDuration.FromUshort(years5);
        
        // Assert
        Assert.Equal(rd, years5);
    }

    [Fact]
    public void The_length_of_the_rental_must_not_be_less_than_1_day()
    {
        // Act
        var exception = Record.Exception(() => RentalDuration.FromUshort(0));
        
        // Assert
        exception.Should().BeOfType<ArgumentException>();
    }
}