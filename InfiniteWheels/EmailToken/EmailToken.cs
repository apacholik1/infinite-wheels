using System.ComponentModel.DataAnnotations;

namespace InfiniteWheels.Infrastructure.EmailToken;

public class EmailToken
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Token { get; set; }
    [Required]
    public string Email { get; set; }
    [Required]
    public DateTime CreatedOn { get; set; }
    public bool HasUsed { get; set; }
    public DateTime UsedOn { get; set; }
}