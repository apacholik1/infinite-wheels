using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;

namespace InfiniteWheels.Infrastructure.EmailToken;

public class EmailTokenService
{
    private InfiniteWheelsDbContext _dbContext;

    public EmailTokenService( InfiniteWheelsDbContext dbContext )
    {
        _dbContext = dbContext;
    }

    public async Task<string> GetTokenFor(string email)
    {

        EmailToken? emailToken = await FindNotUsedToken(email);

        if (emailToken == null)
        {
            emailToken = new EmailToken()
            {
                Token = GenerateToken(),
                Email = email,
                CreatedOn = DateTime.UtcNow
            };

            await _dbContext.EmailTokens.AddAsync(emailToken);
            await _dbContext.SaveChangesAsync();
        }

        return emailToken.Token;
    }

    public async Task<bool> VerifyTokenAndEmail(string email, string token)
    {
        EmailToken? emailToken = await FindNotUsedToken(email);

        if (emailToken == null || emailToken.Token != token)
        {
            return false;
        }

        emailToken.HasUsed = true;
        emailToken.UsedOn = DateTime.UtcNow;
        await _dbContext.SaveChangesAsync();

        return true;
    }

    private string GenerateToken()
    {
        var guid = Guid.NewGuid().ToString();

        return guid.Substring(0, 6).ToUpper();
    }

    private async Task<EmailToken?> FindNotUsedToken(string email)
    {
        return await _dbContext.EmailTokens.FirstOrDefaultAsync((emailToken => 
                !emailToken.HasUsed && emailToken.Email == email
            ));
    }
}