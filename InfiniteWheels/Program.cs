using System.Data.Common;
using System.Text;
using Dapper;
using InfiniteWheels.Domain.Offer;
using InfiniteWheels.Framework;
using InfiniteWheels.Infrastructure;
using InfiniteWheels.Offer;
using Npgsql;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using DotNetEnv;
using InfiniteWheels.EmailSender;
using InfiniteWheels.Infrastructure.EmailToken;
using InfiniteWheels.RefreshToken;

var environmentType = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

if (environmentType != "Production")
{
    Env.TraversePath().Load("../");
}

var environments = Environment.GetEnvironmentVariables();

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEntityFrameworkNpgsql();
string connectionString = $"Host={environments["DB_HOST"]};Database={environments["DB_NAME"]};Username={environments["DB_USER_NAME"]};Password={environments["DB_USER_PASSWORD"]}";
builder.Services.AddPostgresDbContext<InfiniteWheelsDbContext>(connectionString);

builder.Services.AddIdentity<ApplicationUser, IdentityRole>(options =>
    {
        options.Password.RequireDigit = false;
        options.Password.RequireLowercase = false;
        options.Password.RequireUppercase = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequiredLength = 6;
        options.User.RequireUniqueEmail = true;
        options.User.AllowedUserNameCharacters = null;
    })
    .AddEntityFrameworkStores<InfiniteWheelsDbContext>()
    .AddDefaultTokenProviders();

// Configure JWT authentication
var jwtSecret = environments["JWT_SECRET"]?.ToString();
if (string.IsNullOrEmpty(jwtSecret))
{
    throw new Exception("JWT_SECRET environment variable is needed");
}
var key = Encoding.ASCII.GetBytes(jwtSecret);
builder.Services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            // who has the right to use this token
            ValidAudience = builder.Configuration["JWT:Audience"],
            // authentication server
            ValidIssuer = builder.Configuration["JWT:Issuer"],
        };
    });

builder.Services.AddScoped<DbConnection>(c =>
{
    DefaultTypeMap.MatchNamesWithUnderscores = true;
    
    SqlMapper.AddTypeHandler(new UshortTypeHandler());
    
    return new NpgsqlConnection(connectionString);
});

builder.Services.AddScoped<IUnitOfWork, EfCoreUnitOfWork>();
builder.Services.AddScoped<IOfferRepository, OfferRepository>();
builder.Services.AddSingleton<OfferApplicationService>();
builder.Services.AddSingleton<RefreshTokenService>();
builder.Services.AddSingleton<EmailTokenService>();
builder.Services.AddSingleton<EmailSenderService>();
builder.Services.AddControllers(options =>
{
    var policy = new AuthorizationPolicyBuilder()
        .RequireAuthenticatedUser()
        .Build();
    options.Filters.Add(new AuthorizeFilter(policy));
});
builder.Services.AddHealthChecks();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new()
    {
        Title = "Infinite Wheels Service API",
        Version = "v1"
    });
    
    var securityScheme = new OpenApiSecurityScheme
    {
        Name = "JWT Authentication",
        Description = "Enter JWT Bearer token **_only_**",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = "bearer",
        BearerFormat = "JWT",
        Reference = new OpenApiReference
        {
            Type = ReferenceType.SecurityScheme,
            Id = JwtBearerDefaults.AuthenticationScheme
        }
        
    };
    
    c.AddSecurityDefinition("Bearer", securityScheme);
    c.OperationFilter<SecurityRequirementsOperationFilter>();
});

var app = builder.Build();

app.EnsureDatabase();
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Infinite Wheels Service API");

    c.SupportedSubmitMethods(new[]
    {
        SubmitMethod.Get, SubmitMethod.Post, SubmitMethod.Put, SubmitMethod.Delete
    });
});

app.UseHealthChecks(path: "/healt");

// app.UseHttpsRedirection();

app.MapControllers();

app.Run();

public class SecurityRequirementsOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var isTaggedAsAllowedAnonymous = context.MethodInfo.GetCustomAttributes(true)
            .OfType<AllowAnonymousAttribute>().Any();
        
        if (!isTaggedAsAllowedAnonymous)
        {
            operation.Security ??= new List<OpenApiSecurityRequirement>();

            var scheme = new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            };
            
            operation.Security.Add(
                new OpenApiSecurityRequirement
                {
                    [scheme] = new string[] { }
                });
        }
    }
}