using System.Data.Common;
using Dapper;

namespace InfiniteWheels.Offer;

public static class Queries
{
    public static Task<IEnumerable<ReadModels.OfferListItem>> Query(
        this DbConnection connection,
        QueryModels.GetDraftOffers query
    ) => connection.QueryAsync<ReadModels.OfferListItem>(
        """
        SELECT offer_id, state
        FROM offers
        WHERE state=@State
        LIMIT @PageSize OFFSET @Offset
        """,
        new
        {
            State = (int) Domain.Offer.Offer.OfferStatus.Draft,
            PageSize = query.PageSize,
            Offset = Offset(query.Page, query.PageSize),
        }
    );

    public static Task<ReadModels.OfferDetails> Query(
        this DbConnection connection,
        QueryModels.GetOffer query
    ) => connection.QuerySingleAsync<ReadModels.OfferDetails>(
        """
        SELECT state, duration_value as duration, lifting_capacity_value as lifting_capacity
        FROM offers
        WHERE offer_id=@Id
        """,
        new
        {
            Id = query.OfferId,
        }
    );


    private static int Offset(int page, int pageSize) => page * pageSize;
}