using System.Net;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace InfiniteWheels.Offer;

[Tags("Offer")]
[Route("offer")]
public class OfferCommandsApi : Controller
{
    private readonly OfferApplicationService _applicationService;

    public OfferCommandsApi(OfferApplicationService applicationService) =>
        _applicationService = applicationService;
    
    [HttpPost]
    [ProducesResponseType((int) HttpStatusCode.Unauthorized)]
    [ProducesResponseType((int) HttpStatusCode.InternalServerError)]
    public async Task<IActionResult> Post()
    {
        var userId = User.FindFirst((ClaimTypes.NameIdentifier))?.Value;
        
        var clientId = new Guid(userId);

        var offerId = Guid.NewGuid();
        
        await _applicationService.Handle(new CommandModels.V1.Create
        {
            Id = offerId,
            ClientId = clientId,
        });
        
        return Ok(new
        {
            offerId,
        });
    }

    [Route("duration")]
    [HttpPut]
    public async Task<IActionResult> Put(CommandModels.V1.ChangeRentalDuration request)
    {
        await _applicationService.Handle(request);

        return Ok();
    }

    [Route("lifting-capacity")]
    [HttpPut]
    public async Task<IActionResult> Put(CommandModels.V1.ChangeLiftingCapacity request)
    {
        await _applicationService.Handle(request);

        return Ok();
    }

    [Route("sign")]
    [HttpPost]
    public async Task<IActionResult> Post(CommandModels.V1.Accept request)
    {
        await _applicationService.Handle(request);

        return Ok();
    }
}