using InfiniteWheels.Domain.Offer;
using InfiniteWheels.Framework;
using static InfiniteWheels.Offer.CommandModels;

namespace InfiniteWheels.Offer;

public class OfferApplicationService : IApplicationService
{
    private readonly IOfferRepository _repository;
    private readonly IUnitOfWork _unitOfWork;

    public OfferApplicationService(IOfferRepository offerRepository, IUnitOfWork unitOfWork)
    {
        _repository = offerRepository;
        _unitOfWork = unitOfWork;
    }
    public Task Handle(object command) =>
        command switch
        {
            V1.Create cmd => HandleCreate(cmd.Id, cmd.ClientId),
            V1.ChangeRentalDuration cmd => HandleUpdate(cmd.Id, c => c.SetRentalDuration(cmd.RentalDuration)),
            V1.ChangeLiftingCapacity cmd => HandleUpdate(cmd.Id, offer => offer.SetLiftingCapacity(cmd.LiftingCapacity)),
            V1.Accept cmd => HandleUpdate(cmd.Id, c => c.Accept()),
            _ => Task.CompletedTask,
        };

    private async Task HandleCreate(Guid infinityWheelContractId, Guid infinityWheelClientId)
    {
        if (await _repository.Exists(infinityWheelContractId))
            throw new InvalidOperationException(
                $"Encja o id {infinityWheelContractId} juz istnieje");
        
        var contract = new Domain.Offer.Offer(new OfferId(infinityWheelContractId), new ClientId(infinityWheelClientId));
    
        await _repository.Add(contract);
        await _unitOfWork.Commit();
    }
    
    private async Task HandleUpdate(Guid infinityWheelContractId, Action<Domain.Offer.Offer> operation)
    {
    
        Domain.Offer.Offer offer = await _repository.Load(infinityWheelContractId);
    
        if (offer == null)
            throw new InvalidOperationException(
                $"Nie mozna znaleźć encji o id {infinityWheelContractId}");
    
        operation(offer);

        await _unitOfWork.Commit();
    }
}