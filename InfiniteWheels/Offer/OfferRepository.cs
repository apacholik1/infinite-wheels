using InfiniteWheels.Infrastructure;

namespace InfiniteWheels.Offer;

public class OfferRepository : Domain.Offer.IOfferRepository, IDisposable
{
    private readonly InfiniteWheelsDbContext _dbContext;

    public OfferRepository(InfiniteWheelsDbContext dbContext) => _dbContext = dbContext;

    public async Task<bool> Exists(Guid id) => await _dbContext.Offers.FindAsync(id) != null;

    public async Task<Domain.Offer.Offer> Load(Guid id) => await _dbContext.Offers.FindAsync(id);

    public async Task Add(Domain.Offer.Offer entity)
    {
        await _dbContext.Offers.AddAsync(entity);
    }

    public void Dispose() => _dbContext.Dispose();
}