using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InfiniteWheels.Offer;

public static class QueryModels
{
    public class GetOffer
    {
        [Required]
        public Guid OfferId { get; set; }
    }
    
    public class GetDraftOffers
    {
        [Required]
        [DefaultValue(0)]
        public int Page { get; set; }

        [Required]
        [DefaultValue(10)]
        public int PageSize { get; set; }
    }
}