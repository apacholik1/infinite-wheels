namespace InfiniteWheels.Offer;

public class ReadModels
{
    public class OfferDetails
    {
        public int Status { get; set; }
        public ushort? Duration { get; set; }
        public int? LiftingCapacity { get; set; }
    }

    public class OfferListItem
    {
        public Guid OfferId { get; set; }
        public int Status { get; set; }
    }
}