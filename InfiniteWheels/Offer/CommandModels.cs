namespace InfiniteWheels.Offer;

public static class CommandModels
{
    public static class V1
    {
        public class Create
        {
            public Guid Id { get; set; }
            public Guid ClientId { get; set; }
        }
        
        public class ChangeRentalDuration
        {
            public Guid Id { get; set; }
            public ushort RentalDuration { get; set; }
        }

        public class ChangeLiftingCapacity
        {
            public Guid Id { get; set; }
            public uint LiftingCapacity { get; set; }
        }

        public class Accept
        {
            public Guid Id { get; set; }
        }
    }
}