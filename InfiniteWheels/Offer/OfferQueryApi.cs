using System.Data.Common;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace InfiniteWheels.Offer;

[Tags("Offer")]
[Route("offer")]
public class OfferQueryApi : Controller
{
    private readonly DbConnection _connection;

    public OfferQueryApi(DbConnection dbConnection) => _connection = dbConnection;
    
    [HttpGet]
    [Route("list")]
    public async Task<IActionResult> Get(QueryModels.GetDraftOffers request)
    {
        var offersList = await _connection.Query(request);
        return new OkObjectResult(offersList);
    }

    [HttpGet]
    [ProducesResponseType((int) HttpStatusCode.OK)]
    [ProducesResponseType((int) HttpStatusCode.NotFound)]
    public async Task<IActionResult> Get(QueryModels.GetOffer request)
    {
        var offer = await _connection.Query(request);
        return new OkObjectResult(offer);
    }
}