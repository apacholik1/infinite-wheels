using Mailjet.Client;
using Mailjet.Client.Resources;
using Newtonsoft.Json.Linq;

namespace InfiniteWheels.EmailSender;

public class EmailSenderService
{
    private MailjetClient _emailClient;

    public EmailSenderService()
    {
        _emailClient = new MailjetClient(
             Environment.GetEnvironmentVariable("MAIL_SERVICE_TOKEN"),
             Environment.GetEnvironmentVariable("MAIL_SERVICE_SECRET")
        );
    }

    private MailjetRequest RecquestFactory()
    {
        return new MailjetRequest
            {
                Resource = Send.Resource,
            }
            .Property(Send.FromEmail, "no-reply@apacholik.pl")
            .Property(Send.FromName, "InfinityWheels");
    }

    public async Task<bool> SendEmailWithToken(string token, EmailSenderModels.Recipient recipient)
    {
        var request = RecquestFactory();

        var recipientObj = new JObject()
        {
            { "Email", recipient.Email },
        };

        if (!String.IsNullOrEmpty(recipient.Name))
        {
            recipientObj.Add("Name", recipient.Name);
        }
        
        request.Property(Send.MjTemplateID, "5888657")
            .Property(Send.MjTemplateLanguage, "True")
            .Property(Send.Subject, "InfinityWheels | email address confirmation code")
            .Property(Send.Vars, new JObject
            {
                { "code", token }
            })
            .Property(Send.Recipients, new JArray { recipientObj });
        
        MailjetResponse response = await _emailClient.PostAsync(request);
        if (response.IsSuccessStatusCode)
        {
            Console.WriteLine(response.GetData());

            return true;
        }

        Console.WriteLine($"StatusCode: {response.StatusCode}");
        Console.WriteLine($"ErrorInfo: {response.GetErrorInfo()}");
        Console.WriteLine(response.GetData());
        Console.WriteLine($"ErrorMessage: {response.GetErrorMessage()}");

        return false;
    }
}