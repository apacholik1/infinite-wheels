namespace InfiniteWheels.EmailSender;

public static class EmailSenderModels
{
    public class Recipient
    {
        public string Email { get; set; }
        public string? Name { get; set; }
    }
}