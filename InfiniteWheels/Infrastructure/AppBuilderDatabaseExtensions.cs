using Microsoft.EntityFrameworkCore;

namespace InfiniteWheels.Infrastructure
{
    public static class AppBuilderDatabaseExtensions
    {
        public static IApplicationBuilder EnsureDatabase(this IApplicationBuilder builder)
        {
            EnsureContextIsMigrated(builder.ApplicationServices.GetService<InfiniteWheelsDbContext>());
            return builder;
        }

        private static void EnsureContextIsMigrated(DbContext context)
        {
            if (!context.Database.EnsureCreated())
                context.Database.Migrate();
        }

        public static IServiceCollection AddPostgresDbContext<T>(this IServiceCollection services, 
            string connectionString) where T : DbContext
        {
            services.AddDbContext<T>(options => options.UseNpgsql(connectionString).UseSnakeCaseNamingConvention());
            return services;
        }
    }
}