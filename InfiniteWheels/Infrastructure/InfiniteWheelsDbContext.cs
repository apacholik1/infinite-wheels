using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InfiniteWheels.Infrastructure;

public class InfiniteWheelsDbContext : IdentityDbContext<ApplicationUser>
{
    private readonly ILoggerFactory _loggerFactory;

    public InfiniteWheelsDbContext(
        DbContextOptions<InfiniteWheelsDbContext> options,
        ILoggerFactory loggerFactory)
        : base(options) => _loggerFactory = loggerFactory;

    public DbSet<Domain.Offer.Offer> Offers { get; set; }
    public DbSet<RefreshToken.RefreshToken> RefreshTokens { get; set; }
    public DbSet<EmailToken.EmailToken> EmailTokens { get; set; }

    protected override void OnConfiguring(
        DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(_loggerFactory);
        optionsBuilder.EnableSensitiveDataLogging();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<RefreshToken.RefreshToken>();
        
        modelBuilder.ApplyConfiguration(new OfferEntityTypeConfiguration());
    }
}

public class OfferEntityTypeConfiguration
    : IEntityTypeConfiguration<Domain.Offer.Offer>
{
    public void Configure(EntityTypeBuilder<Domain.Offer.Offer> builder)
    {
        builder.HasKey(x => x.OfferId);
        builder.OwnsOne(x => x.ClientId);
        builder.OwnsOne(x => x.Id);
        builder.OwnsOne(x => x.Duration);
        builder.OwnsOne(x => x.LiftingCapacity);
    }
}