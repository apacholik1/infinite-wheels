using InfiniteWheels.Framework;

namespace InfiniteWheels.Infrastructure;

public class EfCoreUnitOfWork : IUnitOfWork
{
    private readonly InfiniteWheelsDbContext _dbContext;

    public EfCoreUnitOfWork(InfiniteWheelsDbContext dbContext)
        => _dbContext = dbContext;

    public Task Commit() => _dbContext.SaveChangesAsync();
}