using System.Data;
using Dapper;

namespace InfiniteWheels.Infrastructure;

public class UshortTypeHandler : SqlMapper.TypeHandler<ushort>
{
    public override ushort Parse(object value)
    {
        return Convert.ToUInt16(value);
    }
    
    public override void SetValue(IDbDataParameter parameter, ushort value)
    {
        parameter.DbType = DbType.UInt16;
        parameter.Value = value;
    }
}