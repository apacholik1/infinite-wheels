using System.Security.Cryptography;
using InfiniteWheels.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace InfiniteWheels.RefreshToken;

public class RefreshTokenService
{
    private readonly InfiniteWheelsDbContext _dbContext;

    public RefreshTokenService(InfiniteWheelsDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<string> GenerateToken(Guid userId)
    {
        string tokenValue;
        var randomNumber = new byte[32];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(randomNumber);
            tokenValue = Convert.ToBase64String(randomNumber).Substring(0, 32);
        }
        
        var refreshToken = new RefreshToken
        {
            Token = tokenValue,
            UserId = userId,
            ExpiryTime = DateTime.UtcNow.AddDays(30),
        };

        await _dbContext.RefreshTokens.AddAsync(refreshToken);
        await _dbContext.SaveChangesAsync();
        
        return tokenValue;
    }

    public async Task<Guid?> FindUserByToken(string token)
    {
        var refreshToken = await _dbContext.RefreshTokens.FirstOrDefaultAsync((x) => x.Token == token);

        if (refreshToken == null || refreshToken.ExpiryTime <= DateTime.UtcNow)
        {
            return null;
        }

        return refreshToken.UserId;
    }
}