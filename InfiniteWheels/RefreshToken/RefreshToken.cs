using System.ComponentModel.DataAnnotations;

namespace InfiniteWheels.RefreshToken;

public class RefreshToken
{
    [Key]
    [MaxLength(32)]
    public string Token { get; set; }
    
    [Required]
    public Guid UserId { get; set; }
    
    [Required]
    public DateTime ExpiryTime { get; set; }
}