using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using InfiniteWheels.EmailSender;
using InfiniteWheels.Infrastructure;
using InfiniteWheels.Infrastructure.EmailToken;
using InfiniteWheels.RefreshToken;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using static InfiniteWheels.Identification.IdentificationModels;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace InfiniteWheels.Identification;

[Tags("Identification")]
[Route("identification")]
public class IdentificationApi : Controller
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RefreshTokenService _refreshTokenService;
    private readonly EmailTokenService _emailTokenService;
    private readonly EmailSenderService _emailSenderService;
    private readonly IConfiguration _configuration;
    
    public IdentificationApi(
        UserManager<ApplicationUser> userManager,
        RefreshTokenService refreshTokenService,
        EmailTokenService emailTokenService,
        EmailSenderService emailSenderService,
        IConfiguration configuration
    )
    {
        _userManager = userManager;
        _refreshTokenService = refreshTokenService;
        _emailTokenService = emailTokenService;
        _emailSenderService = emailSenderService;
        _configuration = configuration;
    }

    [AllowAnonymous]
    [HttpGet("refresh")]
    [ProducesResponseType((int) HttpStatusCode.OK)]
    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetRefresh()
    {
        var token = Request.Cookies["refreshToken"];

        if (token == null)
        {
            return BadRequest();
        }
        
        var userId = await _refreshTokenService.FindUserByToken(token);

        if (userId == null)
        {
            return BadRequest();
        }

        var user = await _userManager.FindByIdAsync(userId.ToString());

        if (user == null)
        {
            return BadRequest();
        }

        return Ok(new
        {
            accessToken = GenerateAccessToken(user),
        });
    }

    [AllowAnonymous]
    [HttpPost("authentication")]
    [ProducesResponseType((int) HttpStatusCode.OK)]
    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
    [ProducesResponseType((int) HttpStatusCode.InternalServerError)]
    public async Task<IActionResult> PostCustomerAuthentication(CustomerAuthentications model)
    {
        if (String.IsNullOrEmpty(model.Token))
        {
            string emailToken = await _emailTokenService.GetTokenFor(model.Email);
            
            bool isFinishWithSuccess = await _emailSenderService.SendEmailWithToken(emailToken, new EmailSenderModels.Recipient()
            {
                Email = model.Email,
            });

            if (!isFinishWithSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
            return Ok();
        }
        
        if (!await _emailTokenService.VerifyTokenAndEmail(model.Email, model.Token))
        {
            return BadRequest(new
            {
                message = "adres e-mail i token nie pasuj� do siebie lub token ju� zosta� u�yty",
            });
        }

        ApplicationUser user = await _userManager.FindByEmailAsync(model.Email);

        if (user == null)
        {
            user = new ApplicationUser { Email = model.Email, UserName = GenerateRandomUsername(), EmailConfirmed = true };
            var result = await _userManager.CreateAsync(user);
        
            if (!result.Errors.IsNullOrEmpty())
            {
                return BadRequest(result.Errors);
            }
        }
        
        var accessToken = GenerateAccessToken(user);
        var refreshToken = await _refreshTokenService.GenerateToken(new Guid(user.Id));
        
        Response.Cookies.Append("refreshToken", refreshToken, new CookieOptions
        {
            Expires = DateTime.UtcNow.AddDays(30),
            HttpOnly = true,
        });
        
        return Ok(new
        {
            access_token = accessToken,
        });
    }
    
    private string GenerateAccessToken(ApplicationUser user)
    {
        var authClaims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        };

        var key = Encoding.ASCII.GetBytes(Environment.GetEnvironmentVariable("JWT_SECRET"));
        var authSigningKey = new SymmetricSecurityKey(key);

        var token = new JwtSecurityToken(
            issuer: _configuration["JWT:Issuer"],
            audience: _configuration["JWT:Audience"],
            expires: DateTime.UtcNow.AddMinutes(15),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
        );

        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    private string GenerateRandomUsername()
    {
        var randomNumber = new byte[8];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }
    }
}