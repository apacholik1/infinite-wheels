using System.ComponentModel.DataAnnotations;

namespace InfiniteWheels.Identification;

public static class IdentificationModels
{
    public class CustomerAuthentications
    {
        [Required]
        public string Email { get; set; }
        public string? Token { get; set; }
    }
}