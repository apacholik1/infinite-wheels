# Infinite Wheels

Jest to projekt edukacyjny aby zapoznać się z C# oraz DD

## Architaktura katalogów

- `InfiniteWheels` - główny projekt w solucji, typu aplikacji sieciowej (restful api)
- `InfiniteWheels.Framework` - elementy pomocnicze
- `InfiniteWheels.Domain` - elementy obsługujące domenę
- `InfiniteWheels.Test` - projekt do obsługi testów