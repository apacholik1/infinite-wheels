using System.Threading.Tasks;

namespace InfiniteWheels.Framework;

public interface IApplicationService
{
    Task Handle(object command);
}