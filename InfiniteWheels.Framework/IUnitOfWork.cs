namespace InfiniteWheels.Framework;

public interface IUnitOfWork
{
    Task Commit();
}