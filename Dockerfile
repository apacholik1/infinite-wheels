FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /app

COPY ./InfiniteWheels/InfiniteWheels.csproj ./InfiniteWheels/InfiniteWheels.csproj
COPY ./InfiniteWheels.Domain/InfiniteWheels.Domain.csproj ./InfiniteWheels.Domain/InfiniteWheels.Domain.csproj
COPY ./InfiniteWheels.Framework/InfiniteWheels.Framework.csproj ./InfiniteWheels.Framework/InfiniteWheels.Framework.csproj
RUN dotnet restore ./InfiniteWheels

COPY . .

RUN dotnet publish ./InfiniteWheels -c Release -o out

# Build the runtime image
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS runtime
WORKDIR /app
COPY --from=build /app/out ./

ENV DB_HOST=""
ENV DB_NAME=""
ENV DB_USER_NAME=""
ENV DB_USER_PASSWORD=""
ENV JWT_SECRET=""
ENV MAIL_SERVICE_TOKEN=""
ENV MAIL_SERVICE_SECRET=""

EXPOSE 80
ENV ASPNETCORE_ENVIRONMENT=Production

ENTRYPOINT ["dotnet", "InfiniteWheels.dll"]